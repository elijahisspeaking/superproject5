// SuperProject5.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <vector>
#include <cmath>


class Vector
{
    public:
    Vector() : x(5), y(5), z(5)
    {}
    Vector(int _x, int _y, int _z) : x(_x), y(_y), z(_z)
    {}
    void Show()
    {
        std::cout << "\n" << x << " " << y << " " << z;
        std::cout << "\n" << "Length of vector: " << sqrt(x * x + y * y + z * z);
    }

    
private:
    int x = 2;
    int y = 2;
    int z = 2;

};


int main()
{
    Vector v;
    v.Show();

    
}
